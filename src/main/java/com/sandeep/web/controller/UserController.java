package com.sandeep.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;

import com.google.gson.Gson;
import com.sandeep.web.dao.UserDao;
import com.sandeep.web.entity.User;

@WebServlet("/registerUser")
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
	private Gson gson;
	private String responseString;

	public void init() {
		userDao = new UserDao();
		gson = new Gson();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		register(request, response);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("register.jsp");
	}

	private void register(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(request.getReader().readLine());
			User user = (User) gson.fromJson(sb.toString(),User.class);
			String regex = "^(.+)@(.+)$";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(user.getEmail());
			if(user.getName().equals("")){
				System.out.println("null name");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(!user.getName().matches("^[a-zA-Z ]*$")){
				System.out.println("invalid name");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(user.getUsername().equals("")){
				System.out.println("null username");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(!user.getUsername().matches("^[a-zA-Z0-9_.]{5,}$")){
				System.out.println("invalid username");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(user.getMobile()<6000000000L){
				System.out.println("invalid mobile");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(user.getEmail().equals("")){
				System.out.println("null email");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(!matcher.matches()){
					System.out.println("invalid email");
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(user.getPassword().equals("")){
				System.out.println("null password");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(user.getPassword().length()<6){
				System.out.println("invalid password");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
			if(response.getStatus()!=HttpServletResponse.SC_BAD_REQUEST){
				user.setPassword(hashPassword(user.getPassword()));
				if(userDao.saveUser(user)){
					responseString = "{\"status\": \"success\", \"message\": \"Thanks for Registering, You can login now\"}";
				}else {
					responseString = "{\"status\": \"error\", \"message\": \"user mobile/username/email already registered\"}";
				}
			}else {
				responseString = "{\"status\": \"error\", \"message\": \"incorrect data\"}";
			}
		}catch(Exception e){
			System.out.println(e);
			responseString = "{\"status\": \"error\", \"message\": \"incorrect data\"}";
		}
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(responseString);
		out.flush();
	}
		
		// String name = request.getParameter("name");
		// String username = request.getParameter("username");
		// String email = request.getParameter("email");
		// long mobile = Long.parseLong(request.getParameter("mobile"));
		// String password = hashPassword(request.getParameter("password"));

		// User user = new User();
		// user.setName(name);
		// user.setUsername(username);
		// user.setEmail(email);
		// user.setMobile(mobile);
		// user.setPassword(password);
		
		// if(userDao.saveUser(user)){
		// }else{
		// 	response.sendRedirect("register-user.jsp");
		// }

	private String hashPassword(String plainTextPassword){
		return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
	}
}