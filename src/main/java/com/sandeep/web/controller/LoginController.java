package com.sandeep.web.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.mindrot.jbcrypt.BCrypt;

import com.sandeep.web.entity.User;
import com.sandeep.web.dao.UserDao;

@WebServlet("/login")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
	private User user;

	public void init() {
		userDao = new UserDao();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processLoginRequest(request, response);
	}

	private void processLoginRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		user = userDao.fetchUser(username);
		if(username == null || username.equals("")){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}else if(password == null || password.equals("")){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}else if(user == null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}else if(!BCrypt.checkpw(password, user.getPassword())) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		if(response.getStatus()!=HttpServletResponse.SC_BAD_REQUEST){
			HttpSession session = request.getSession();
			session.setAttribute("username",username);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin");
			requestDispatcher.forward(request,response);
		}
		else{
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
			requestDispatcher.forward(request, response);
		}
	}
}