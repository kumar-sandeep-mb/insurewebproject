package com.sandeep.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import com.sandeep.web.dao.QueryDao;
import com.sandeep.web.entity.Query;

@WebServlet("/registerQuery")
public class QueryController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private QueryDao queryDao;
	private Gson gson;
	private String responseString;
	private String failureString;
	private String successString;

	public void init() {
		queryDao = new QueryDao();
		gson = new Gson();
		failureString = "{\"status\": \"error\", \"message\": \"incorrect data\"}";
		successString = "{\"status\": \"success\", \"message\": \"Thanks for contacting us. We will get back to you soon\"}";	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		registerQuery(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("contact.jsp");	
	}

	private void registerQuery(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(request.getReader().readLine());
			Query query = (Query) gson.fromJson(sb.toString(),Query.class);
			String regex = "^(.+)@(.+)$";
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = pattern.matcher(query.getEmail());
			if(query.getName().equals("")){
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(!query.getName().matches("^[a-zA-Z\\s]*$")){
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(query.getEmail().equals("")){
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if(!matcher.matches()){
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
			if(response.getStatus()!=HttpServletResponse.SC_BAD_REQUEST){
				queryDao.saveQuery(query);
				responseString = successString;
			}else{
				responseString = failureString;
			}
		}catch(Exception e){
			System.out.println(e);
			responseString = failureString;
		}
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(responseString);
		out.flush();
	}
}