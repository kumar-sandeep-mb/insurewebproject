package com.sandeep.web.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sandeep.web.dao.QueryDao;
import com.sandeep.web.entity.Query;

@WebServlet("/admin")
public class AdminController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    QueryDao queryDao;
	public void init() {
        queryDao = new QueryDao();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processQueryRequest(request, response);
	}

	private void processQueryRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Query> queryList = queryDao.fetchQueryData();
		request.setAttribute("queryList", queryList);
		// Query query = new Query();
		// query.setEmail("sandysingh1918@gmail.com");
		// query.setMessage("hello world");
		// query.setName("sandeep");
		// Gson gson = new Gson();
		// String json = gson.toJson(query);
		// PrintWriter out = response.getWriter();
		// response.setContentType("application/json");
		// response.setCharacterEncoding("UTF-8");
		// out.print(json);
		// out.flush();
		RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
		dispatcher.forward(request, response);
	}
}