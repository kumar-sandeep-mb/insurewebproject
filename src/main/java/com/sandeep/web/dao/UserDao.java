package com.sandeep.web.dao;

import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.sandeep.web.entity.User;

public class UserDao {
	public boolean saveUser(User user) {
		try{
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("sample");
			EntityManager em = emf.createEntityManager();
			em.getTransaction().begin();
			em.persist(user);
			em.getTransaction().commit();
			em.close();
			emf.close();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public User fetchUser(String username){
		User user = null;
		try{
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("sample");
			EntityManager em = emf.createEntityManager();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<User> cq = cb.createQuery(User.class);
			Root<User> admin = cq.from(User.class);
			cq.where(cb.equal(admin.get("username"), username));
			CriteriaQuery<User> select = cq.select(admin);  
			TypedQuery<User> query = em.createQuery(select);
			user = query.getSingleResult();
			em.close();
			emf.close();
        }catch(Exception e)
        {
			System.out.println(e);
        }
		return user;
	}
}