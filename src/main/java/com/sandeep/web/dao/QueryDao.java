package com.sandeep.web.dao;

import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import com.sandeep.web.entity.Query;

public class QueryDao {
	public boolean saveQuery(Query query) {
		try{
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("sample");
			EntityManager em = emf.createEntityManager();
			em.getTransaction().begin();
			em.persist(query);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public List<Query> fetchQueryData(){
		List<Query> list = null;
		try{
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("sample");
			EntityManager em = emf.createEntityManager();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Query> cq = cb.createQuery(Query.class);
			Root<Query> from = cq.from(Query.class);
			CriteriaQuery<Query> select = cq.select(from);
			TypedQuery<Query> query = em.createQuery(select);
			list = query.getResultList();
			em.close();
			emf.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return list;
	}
}