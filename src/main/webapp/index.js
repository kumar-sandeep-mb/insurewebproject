function opendropdown(){
    var menubutton=document.getElementById("menu");
    var closebutton=document.getElementById("close");
    var list=document.getElementById("list");
    menubutton.style.display="none";
    closebutton.style.display="block";
    list.style.display="block";
}

function closedropdown(){
    var menubutton=document.getElementById("menu");
    var closebutton=document.getElementById("close");
    var list=document.getElementById("list");
    menubutton.style.display="block";
    closebutton.style.display="none";
    list.style.display="none";
}

function validateQuery(form) {
    if(!form.name.value.match(/^[a-z][a-z\s]*$/)){
        alert("invalid name!! should not contain numeric values and special characters")
        return false;
    }
    if(!form.email.value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
        alert("invalid email");
        return;
    }
    sendQueryToServer(form);
}

function sendQueryToServer(form){
    var query = {
        name : form.name.value,
        email : form.email.value,
        message : form.message.value
    };
    fetch('/registerQuery', {
        method: 'POST', 
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(query),
        }).then(function(response) {
          return response.json();
        })
        .then(function(responseAsJson) {
          alert(responseAsJson.status+' : '+responseAsJson.message);
    })
}

function validateUser(form){
    if(!form.name.value.match(/^[a-z][a-z\s]*$/)){
        alert("invalid name!! should not contain numeric values and special characters")
        return false;
    }
    if(!form.username.value.match(/^[a-zA-Z0-9_.]{6,}$/)){
        alert("invalid username!! should be alphanumeric and greater than 5")
        return false;
    }
    if(!form.email.value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
        alert("invalid email");
        return false;
    } 
    if(!form.mobile.value.match(/^([0-9]{1})([0-9]{9})$/)){
        alert("invalid number, should not be text and less than 10 digit and start with 6-9");
        return false;
    }
    if(form.password.value.length<6){
        alert("invalid password!!! length should be greater than 5");
        return false;
    }
    sendUserToServer(form);
}

function sendUserToServer(form){
    var user = {
        name : form.name.value,
        username : form.username.value,
        mobile : form.mobile.value,
        email : form.email.value,
        password : form.password.value
    };
    fetch('/registerUser', {
        method: 'POST', 
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
        }).then(function(response) {
          return response.json();
        })
        .then(function(responseAsJson) {
          alert(responseAsJson.status+' : '+responseAsJson.message);
    })
}

function validateAdmin(form) {
    if(!form.username.value.match(/^[a-zA-Z0-9_.]{6,}$/)){
        alert("invalid username!! should be alphanumeric and greater than 5")
        return false;
    }
    if(form.password.value.length<6){
        alert("invalid password!!! length should be greater than 5");
        return false;
    }
}