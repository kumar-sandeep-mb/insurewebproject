<header class="container">
    <div class="logo">
        <img src="images/logo.svg">
    </div>    
    <div class="nav">
        <ul>
            <li class="headerlink">
                <a href="index.jsp">HOME</a>
            </li>
            <%
            if(session.getAttribute("username")!=null){%>
            <li class="headerlink">
                <a href="logout.jsp">LOGOUT</a>
            </li>
            <%
            }else{
            %>
            <li class="headerlink">
                <a href="login.jsp">LOGIN</a>
            </li>
            <li class="headerlink">
                <a href="register-user.jsp">REGISTER</a>
            </li>
            <li class="headerlink">
                <a href="contact.jsp">CONTACT US</a>
            </li>
            <%}%>
            <li class="headerlink">
                <button class="header_button">VIEW PLANS</button>
            </li>
            <li id="menu">
                <button onclick="opendropdown()"><img src="images/menu.svg"></button>
            </li>
            <li id="close">
                <button onclick="closedropdown()"><img src="images/close.svg"></button>
            </li>
        </ul>
    </div>
</header>
