<%
if(session.getAttribute("username")!=null)
{
    response.sendRedirect("/admin");
}
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <jsp:include page="common-stylesheets.jsp"/>
    <link rel="stylesheet" href="style/login.css">
    <script src="index.js"></script>
</head>
    <body>
        <jsp:include page="header.jsp"/>
        <form id ="form" action="<%=request.getContextPath()%>/login" method="get" onsubmit="validateAdmin(this);">
            <jsp:include page="mobile-menu-box.jsp"/>
            <div class="section">
                <h1>Log In</h1>
                <label>Username</label>
                <input type="text" name="username" required>
                <label>Password</label>
                <input type="password" name="password" required>
                <input type="submit" value="Login">
            </div>          
        </form>
        <jsp:include page="footer.jsp"/>
    </body>
</html>
