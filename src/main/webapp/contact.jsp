<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <jsp:include page="common-stylesheets.jsp"/>
    <link rel="stylesheet" href="style/contact.css">
    <script src="index.js"></script>
</head>
    <body>
        <jsp:include page="header.jsp"/>
        <form id="form" onsubmit="event.preventDefault(); validateQuery(this);">
            <jsp:include page="mobile-menu-box.jsp"/>
            <div class="section">
                <h1>Contact Us</h1>
                <p>Feel free to contact us</p>
                <label>Your name</label>
                <input type="text" name="name" id="name" required>
                <label>Your E-mail</label>
                <input type="email" name="email" id="email" required>
                <label>Message</label>
                <input type="text" name ="message" id="message" required>
                <input type="submit" value="submit" id="submit">  
            </div>          
        </form>
	<jsp:include page="footer.jsp"/>
    </body>
</html>


