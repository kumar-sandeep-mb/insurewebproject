<html>
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title>
            INDEX
        </title>
        <jsp:include page="common-stylesheets.jsp"/>
        <link rel="stylesheet" href="style/index.css">
        <script src="index.js"></script>
    </head>
    <body>
	<jsp:include page="header.jsp"/>     
	    <section>
            <div class="upper-section">
                <jsp:include page="mobile-menu-box.jsp"/>
                <div class="upper-section-first">
                    <img src="images/bg2.jpg">
                </div>
                <div class="upper-section-second">
                    <hr>
                    <h1>
                        Humanizing your insurance.
                    </h1>
                    <p>
                        Get your life insurance coverage easier and faster. We blend our expertise and technology to help you find the plan that's right for you. Ensure you and your loved ones are protected.
                    </p>
                    <button class="upper-section-button">
                        VIEW PLANS
                    </button>
                </div>
            </div>
            <div class="middle-section">
                <div class="container">
                    <div class="heading">
                        <hr>
                        <h1>
                            We're different
                        </h1>
                    </div>
                    <div class="box">
                        <div class="boxes">
                            <img src="images/bg4.svg">
                            <h2>
                                Snappy Process
                            </h2>
                            <p>
                                Our appliction process can be completed in minutes, not hours. Don't get stuck filling in tedious forms.
                            </p>
                        </div>
                        <div class="boxes">
                            <img src="images/bg5.svg">
                            <h2>
                                Affordable Prices
                            </h2>
                            <p>
                                We don't want you worrying about high monthly costs. Our prices may be low, but still we offer the best coverage possible.
                            </p>
                        </div>
                        <div class="boxes">
                            <img src="images/bg6.svg">
                            <h2>
                                People First
                            </h2>
                            <p>
                                Our plans aren't full of conditions and clauses to prevent payouts. We make sure you're covered when you need it.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lower-section">
                <div class="container">
                    <div class="lower-section-first">
                        <h1>
                            Find out more about how we work
                        </h1>
                    </div>
                    <div class="lower-section-second">
                        <button class="lower-section-button">
                            HOW WE WORK
                        </button>
                    </div>
                </div>
            </div>
        </section>
	<jsp:include page="footer.jsp"/>
    </body>
</html>
