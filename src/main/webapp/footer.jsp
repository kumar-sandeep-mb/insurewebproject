<footer>
    <div class="container">
        <div class="footer-header">
            <div class="footer-icon">
                <img src="images/logo.svg">
            </div>
            <div class="footer-nav">
                <img src="images/fb.svg">
                <img src="images/tw.svg">
                <img src="images/pin.svg">
                <img src="images/ig.svg">
            </div>
        </div>
        <hr>
        <div class="footer-box">
            <div>
                <h3>
                    OUR COMPANY
                </h3>
                <ul>
                    <li>
                        <a href="index.html">HOW WE WORK</a>
                    </li>
                    <li>
                        <a href="index.html">WHY INSURE?</a>
                    </li>
                    <li>
                        <a href="index.html">VIEW PLANS</a>
                    </li>
                    <li>
                        <a href="index.html">REVIEWS</a>
                    </li>
                </ul>
            </div>
            <div>
                <h3>HELP ME</h3>
                <ul>
                    <li>
                        <a href="index.html">FAQs</a>
                    </li>
                    <li>
                        <a href="index.html">TERMS OF USE</a>
                    </li>
                    <li>
                        <a href="index.html">PRIVACY POLICY</a>
                    </li>
                    <li>
                        <a href="index.html">COOKIES</a>
                    </li>
                </ul>
            </div>
            <div>
                <h3>CONTACT</h3>
                <ul>
                    <li>
                        <a href="index.html">SALES</a>
                    </li>
                    <li>
                        <a href="index.html">SUPPORT</a>
                    </li>
                    <li>
                        <a href="index.html">LIVE CHAT</a>
                    </li>
                    <li>
                        <a href="contact.jsp">CONTACT US</a>
                    </li>
                </ul>
            </div>
            <div>
                <h3>OTHERS</h3>
                <ul>
                    <li>
                        <a href="index.html">CARRIERS</a>
                    </li>
                    <li>
                        <a href="index.html">PRESS</a>
                    </li>
                    <li>
                        <a href="index.html">LICESNSES</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
