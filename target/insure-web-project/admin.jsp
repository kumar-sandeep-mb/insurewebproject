<%
if(session.getAttribute("username")==null||session.getAttribute("username").equals(""))
{
    response.sendRedirect("login.jsp");
}
else{
%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sandeep.web.entity.Query"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style/common.css">
    <link rel="stylesheet" href="style/header.css">
    <link rel="stylesheet" href="style/footer.css">
    <link rel="stylesheet" href="style/admin.css">
    <script src="index.js"></script>
</head>
    <body>
        <jsp:include page="header.jsp"/>
        <jsp:include page="mobile-menu-box.jsp"/>
        <section>
        <table id="queryTable">
            <tr>
                <th>
                    S.No
                </th>
                <th>
                    NAME
                </th>
                <th>
                    E-MAIL
                </th>
                <th>
                    MESSAGE
                </th>
            </tr>
            <% 
            ArrayList queryList = (ArrayList)request.getAttribute("queryList");
            for(int i = 0 ; i < queryList.size() ; i ++){
                if(i<queryList.size()){
                Query query = (Query)queryList.get(i);
                %>
                <tr>
                    <td>
                        <% out.println(i+1); %>
                    </td>
                    <td>
                        <% out.println(query.getName()); %>
                    </td>
                    <td>
                        <% out.println(query.getEmail()); %>
                    </td>
                    <td>
                        <% out.println(query.getMessage()); %>
                    </td>
                </tr>
            <%
                }}}
            %>
        </table>
        </section>
        <div id="one"></div>
        <jsp:include page="footer.jsp"/>
    </body>
</html>
