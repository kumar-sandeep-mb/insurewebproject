<%
if(session.getAttribute("username")!=null)
{
    response.sendRedirect("/admin");
}
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <jsp:include page="common-stylesheets.jsp"/>
    <link rel="stylesheet" href="style/register.css">
    <script src="index.js"></script>
</head>
    <body>
        <jsp:include page="header.jsp"/>
        <form id="form" onsubmit="event.preventDefault(); validateUser(this);">
            <jsp:include page="mobile-menu-box.jsp"/>
            <div class="section">
                <h1>Register</h1>
                <label>Name</label>
                <input type="text" name="name" required>
                <label>Username</label>
                <input type="text" name="username" required>
                <label>E-mail</label>
                <input type="text" name="email" required>
                <label>Mobile</label>
                <input type="text" name="mobile" required>
                <label>Password</label>
                <input type="text" name="password" required>
                <input type="submit" value ="submit">  
            </div>          
        </form>
        <jsp:include page="footer.jsp"/>
    </body>
</html>
